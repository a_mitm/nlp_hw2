#!/usr/local/bin/python

from data_utils import utils as du
import numpy as np
import pandas as pd
import csv
import code
from sklearn.model_selection import ParameterGrid
# Load the vocabulary
vocab = pd.read_table("data/lm/vocab.ptb.txt", header=None, sep="\s+",
                     index_col=0, names=['count', 'freq'], )

# Choose how many top words to keep
vocabsize = 2000
num_to_word = dict(enumerate(vocab.index[:vocabsize]))
word_to_num = du.invert_dict(num_to_word)

# Load the training set
docs_train = du.load_dataset('data/lm/ptb-train.txt')
S_train = du.docs_to_indices(docs_train, word_to_num)
docs_dev = du.load_dataset('data/lm/ptb-dev.txt')
S_dev = du.docs_to_indices(docs_dev, word_to_num)


def train_ngrams(dataset):
    """
        Gets an array of arrays of indexes, each one corresponds to a word.
        Returns trigram, bigram, unigram and total counts.
    """
    trigram_counts = dict()
    bigram_counts = dict()
    unigram_counts = dict()
    token_count = 0
    ### YOUR CODE HERE
    from collections import defaultdict
    trigram_counts = defaultdict(lambda : 0)
    bigram_counts = defaultdict(lambda : 0)
    unigram_counts = defaultdict(lambda : 0)

    for line in dataset:
        token_count += len(line) - 2

        for i in range(1,len(line)):
            c = num_to_word[line[i]]
            c1 = num_to_word[line[i-1]]
            
            unigram_counts[c] += 1
            bigram_counts[(c1,c)] += 1

            if i > 1:
                c2 = num_to_word[line[i-2]]
                trigram_counts[(c2,c1,c)] += 1
    ### END YOUR CODE
    return trigram_counts, bigram_counts, unigram_counts, token_count

def evaluate_ngrams(eval_dataset, trigram_counts, bigram_counts, unigram_counts, train_token_count, lambda1, lambda2):
    """
    Goes over an evaluation dataset and computes the perplexity for it with
    the current counts and a linear interpolation
    """
    perplexity = 0
    ### YOUR CODE HERE
    #M = len(num_to_word)
    #code.interact(local=dict(globals(), **locals()))
    def safe_log2(x, min_val=0.000000001):
        return np.log2(x.clip(min=min_val))
    perplexity = np.array([])
    perplexity1 = np.array([])
    perplexity2 = np.array([])
    perplexity3 = np.array([])
    
    #eval_dataset =[[item for sublist in eval_dataset for item in sublist]]
    for line in eval_dataset:
        for j in xrange(2,len(line)):
            c = num_to_word[line[j]]
            c1 = num_to_word[line[j-1]]
            c2 = num_to_word[line[j-2]]
            
            p1 = float(unigram_counts[c])/train_token_count
            p2 = float(bigram_counts[(c1,c)])/unigram_counts[c1] if unigram_counts[c1] != 0 else 0
            p3 = float(trigram_counts[(c2,c1,c)])/bigram_counts[(c2,c1)] if bigram_counts[(c2,c1)] != 0 else 0
            perplexity = np.append(perplexity, [lambda1*p3+lambda2*p2+(1-lambda1-lambda2)*p1])
            perplexity1 = np.append(perplexity1, p1)
            perplexity2 = np.append(perplexity2, p2)
            perplexity3 = np.append(perplexity3, p3)

    perplexity = pow(2, -1 * np.average(safe_log2(perplexity)))
    """    
    param_grid = {'i': list(np.arange(0.0,1.0,0.01)), 'j' : list(np.arange(0.0,1.0,0.01))}
    grid = ParameterGrid(param_grid)
    min_param = ()
    min_val = 1000

    for params in grid:
        i, j = params['i'], params['j']
        if i+j>1.0:
            continue
        linear_sum = i*perplexity3 + j*perplexity2+(1-i-j)*perplexity1
        #linear_log = np.log2(linear_sum) if len(np.where(linear_sum == 0)[0])>0 else -np.inf
        #print "parameters : {},{},{}, perplexity: {}".format(i,j,1-i-j,  pow(2, -1 * np.average(safe_log2(linear_sum))))
        perplexity = pow(2, -1 * np.average(safe_log2(linear_sum)))
        if perplexity < min_val:
            min_val = perplexity
            min_param = (i,j,1-i-j, perplexity)
    print "parameters : {},{},{}, perplexity: {}".format(min_param[0], min_param[1], min_param[2], min_param[3])
    #code.interact(local=dict(globals(), **locals()))
""" 
    ### END YOUR CODE
    return perplexity

def test_ngram():
    """
    Use this space to test your n-gram implementation.
    """
    #Some examples of functions usage
    trigram_counts, bigram_counts, unigram_counts, token_count = train_ngrams(S_train)
    print "#trigrams: " + str(len(trigram_counts))
    print "#bigrams: " + str(len(bigram_counts))
    print "#unigrams: " + str(len(unigram_counts))
    print "#tokens: " + str(token_count)
    perplexity = evaluate_ngrams(S_dev, trigram_counts, bigram_counts, unigram_counts, token_count, 0.5, 0.4)
    print "#perplexity: " + str(perplexity)
    ### YOUR CODE HERE
    
    ### END YOUR CODE

if __name__ == "__main__":
    test_ngram()

